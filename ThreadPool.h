#pragma once
#include <vector>
#include <queue>
#include <memory>
#include <functional>
#include <stdexcept>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class ThreadPool {
private:
    // need to keep track of threads so we can join them
    std::vector<boost::thread> workers;
    // the task queue
    std::queue< boost::function<void()>> tasks;
    // synchronization
    boost::mutex queue_mutex;
    boost::condition_variable condition;
    bool stop;

	
public:
    ThreadPool(size_t);
    void enqueue(boost::function<void()> &task);
    ~ThreadPool();
};

// the constructor just launches some amount of workers
ThreadPool::ThreadPool(size_t threads)
    :   stop(false)
{
    for(size_t i = 0;i<threads;++i)
        workers.emplace_back(
			[this] {
                for(;;) {
                    boost::function<void()> task;
                    {
                        boost::unique_lock<boost::mutex> lock(this->queue_mutex);
                        this->condition.wait(lock, [this]
											 { return this->stop || !this->tasks.empty(); });
                        if(this->stop && this->tasks.empty())
                            return;
                        task = std::move(this->tasks.front());
                        this->tasks.pop();
                    }
                    task();
                }
            }
        );
}

void ThreadPool::enqueue(boost::function<void()> &task) 
{
    {
        boost::unique_lock<boost::mutex> lock(queue_mutex);

        // don't allow enqueueing after stopping the pool
        if(stop)
            throw std::runtime_error("enqueue on stopped ThreadPool");
        tasks.emplace(task);
    }
    condition.notify_one();
}

// the destructor joins all threads
ThreadPool::~ThreadPool()
{
    {
        boost::unique_lock<boost::mutex> lock(queue_mutex);
        stop = true;
    }
    condition.notify_all();
    for(boost::thread &worker: workers)
        worker.join();
}
