##################
import socket
import sys
import subprocess
import time
from multiprocessing import Pool

port = 3241

def check_echo(msg):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(("localhost", port))
        print "send: '", msg, "'"
        s.send(msg)
        try:
            res = s.recv(4096)
            print "'",
            print res,
            print "'"
            return res
        except Exception:
            print "'Connection reset by peer'"
    except Exception:
        print "'Connection refused'"


def test_single_connection():
    msg = "set user_123 888 1445128601 10\r\nnanananana\r\n"
    res = check_echo(msg)
    assert(res == "STORED\r\n")
    msg = "set user_223 812 1445128301 10\r\nnannnanana\r\n"
    res = check_echo(msg)
    assert(res == "STORED\r\n")
    msg = "add user_223 123423424 1445428821 10\r\nabcdefghij\r\n"
    res = check_echo(msg)
    assert(res == "NOT_STORED\r\n")
    msg = "delete user_123\r\n"
    res = check_echo(msg)
    assert(res == "DELETED\r\n")
    msg = "get user_123 user_223 qwe\r\n"
    res = check_echo(msg)
    assert(res == "VALUE user_223 812 1445128301 10\r\nnannnanana\r\nEND\r\n")

def test_multiple_connection(seed):
    
    msg = "set " +str(seed)+ " 888 1445128601 10\r\nnanananana\r\n"
    check_echo(msg)
    msg = "get " +str(seed)+ " qwe\r\n"
    res = check_echo(msg)
    assert(res == "VALUE " +str(seed)+ " 888 1445128601 10\r\nnanananana\r\nEND\r\n")



if __name__ == "__main__":
    port = int(sys.argv[1])
    mc = subprocess.Popen(['./server', sys.argv[1]])
    time.sleep(1)
    print "test_single_connection"
    test_single_connection()

    print "test_multiple_connection"
    pool = Pool(processes=2)
    seq = [x for x in range(10)]
    pool.map(test_multiple_connection, seq)
    pool.close()
    pool.join()
    mc.kill()
