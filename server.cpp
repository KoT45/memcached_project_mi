#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream>
#include <vector>
#include <string.h>
#include <unistd.h> 
#include <protocol.h>
#include <ThreadPool.h>
#include <signal.h>
#include <fcntl.h>
#include <set>
#include <atomic>
#include <mutex>


const size_t threads_number = 3;
const size_t cache_size = 10000;
const size_t MAX_EPOLL_EVENTS = 1000;
const size_t BACK_LOG = 1000;


class Server {
private:
	int status;
	struct addrinfo hints;
	struct addrinfo *servinfo;   // will point to the results
	const char *port;
	int listensd;
	int efd;
    std::mutex m;
    std::set<int> fds;
	int events_cout = 1;


	void (*handler)(int, LRU_cache<std::string, std::pair<unsigned int, std::vector<char>>>&, std::set<int>* , std::mutex&);
	ThreadPool TPool;
	LRU_cache<std::string, std::pair<unsigned int, std::vector<char>>> cache;
	
public:
	Server(const char* port, void (*handler)(int, LRU_cache<std::string, std::pair<unsigned int, std::vector<char>>>&, std::set<int>* , std::mutex&)) : port(port), handler(handler), TPool(threads_number), cache(cache_size) {
		signal(SIGPIPE, SIG_IGN);
		efd = epoll_create(MAX_EPOLL_EVENTS);

		memset(&hints, 0, sizeof hints); // make sure the struct is empty
		hints.ai_family = AF_INET;     // IPv4
		hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
		hints.ai_flags = AI_PASSIVE;     // fill in my IP for me
		
		if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
			fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
			exit(1);
		}
		
		// use servinfo
		listensd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
        //setnonblocking(listensd);		
		if (listensd == -1) {
			perror("SOCKET ERROR");
			exit(1);
		}
    
		if (bind(listensd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
			perror("SOCKET ERROR");
			exit(1);
		}
		
	}

	~Server() {
		freeaddrinfo(servinfo); // free the linked-list
	}

    
	int setnonblocking(int sock) {
		int opts;
	    opts = fcntl(sock,F_GETFL);
		if (opts < 0)
		{
			perror("fcntl(F_GETFL)");
				return -1;
		}
		opts = (opts | O_NONBLOCK);
		if (fcntl(sock,F_SETFL,opts) < 0)
		{
				perror("fcntl(F_SETFL)");
				return -1;
		}
		
		
		return 0;
	}	

	int run() {
		struct epoll_event listenev;
		struct epoll_event events[MAX_EPOLL_EVENTS];
		struct epoll_event connev;
	
		
		if (listen(listensd, BACK_LOG) == -1) {
			perror("SOCKET ERROR");
			exit(1);
		}
		listenev.events = EPOLLIN | EPOLLRDHUP;
		listenev.data.fd = listensd;
		if (epoll_ctl(efd, EPOLL_CTL_ADD, listensd, &listenev) < 0) {
			perror("Epoll fd add");
			exit(1);
		}
	    
		struct sockaddr_storage conn_addr;
		unsigned int addr_size = sizeof conn_addr;  
        std::cout << "listensd = " << listensd << std::endl;
		while(true) {
			int nfds = epoll_wait(efd, events, MAX_EPOLL_EVENTS, -1);
			for (int n = 0; n < nfds; ++n) {
				if (events[n].data.fd == listensd)
				{
					addr_size = sizeof(conn_addr);
					int conn_fd = accept(listensd, (struct sockaddr *)&conn_addr, &addr_size);
					std::cout << "NEW CONNECTION!!!\n";
					if (conn_fd < 0)
					{
						perror("accept");
						continue;
					}
					
					if (events_cout == MAX_EPOLL_EVENTS-1)
					{
						std::cout << "Event array is full" << std::endl;
						close(conn_fd);
						continue;
					}/*
                    if (setnonblocking(conn_fd) == -1) {
						std::cout << "SetNonBlocking error" << std::endl;
						close(conn_fd);
						continue;
					}*/

					connev.data.fd = conn_fd;
					connev.events = EPOLLIN | EPOLLRDHUP;
					if (!epoll_ctl(efd, EPOLL_CTL_ADD, conn_fd, &connev) < 0)
					{
						perror("Epoll fd add");
						close(conn_fd);
						continue;
					}

	     			++events_cout;

				} else {
					int conn_fd = events[n].data.fd;
                    m.lock();
                    bool not_working = (fds.find(conn_fd) == fds.end());
                    m.unlock();
					if (events[n].events & EPOLLRDHUP) {
						epoll_ctl(efd, EPOLL_CTL_DEL, conn_fd, &connev);
						close(conn_fd);
						--events_cout;
						std::cout << "SERVER: CLOSE SOCKET=" << conn_fd << std::endl;
					} else if ((events[n].events & EPOLLIN) && ) {
                    
                        fds.insert(conn_fd);
                        std::cout << "SERVER: MAKE TASK SOCKET=" << conn_fd << std::endl;
					    boost::function<void()> task = [&, conn_fd]()
						    {handler(conn_fd, cache, &fds, m);};
                        m.lock();
					    TPool.enqueue(task);
                        m.unlock();
					}
                    
				}
			}
			
		}
	}
	
};


int main(int argc, char *argv[]) {
	try {						
		Server serv(argv[1], &ProcessConnection); 
		serv.run();
	} catch(...) {
	   std::cout << "SERVER: OSHIBKA!!! ALARM!!! OSHIBKA!!" << std::endl;
	   exit(1);
   }
}
