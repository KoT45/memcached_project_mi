#include "gtest/gtest.h"
#include "LRU_cache.h"
#include <vector>
#include <ctime>

/*
  Это не правильные тесты. Чтобы проверять руками.
*/


TEST(LRU_cache, set) {
	LRU_cache<int, int> cache(5);
	cache.print();
	cache.set(1, 2, 3);
	cache.print();
	cache.set(2, 3, 4);
	cache.print();
	cache.set(3, 4, 5);
	cache.print();
	cache.set(4, 5, 6);
	cache.print();
	cache.set(5, 6, 7);
	cache.print();
	cache.set(6, 7, 8);
	cache.print();
}


TEST(LRU_cache, get) {
	LRU_cache<int, int> cache(5);
	cache.set(1, 2, 3);
	cache.set(2, 3, 4);
	cache.set(3, 4, 5);
	cache.set(4, 5, 6);
	cache.set(5, 6, 7);
	cache.set(6, 7, 8);
	cache.print();
	std::vector<int> v(1, 0);
	std::vector<time_t> t(1, 0);
	bool b = cache.get(3, &v[0], &t[0]);
	if (b)
		std::cout << v[0] << ' ' << t[0] << '\n';
	b = cache.get(10, &v[0], &t[0]);
	if (!b)
		std::cout << "OK!\n";
	cache.print();
	
}

TEST(LRU_cache, del) {
LRU_cache<int, int> cache(5);
	cache.set(1, 2, 3);
	cache.set(2, 3, 4);
	cache.set(3, 4, 5);
	cache.set(4, 5, 6);
	cache.set(5, 6, 7);
	cache.set(6, 7, 8);
	cache.print();
	cache.del(3);
	cache.print();
	cache.del(6);
	cache.print();
	cache.del(2);
	cache.print();
}

TEST(LRU_cache, time) {
	LRU_cache<int, int> cache(20000);
	std::cout << "SET TIME TEST:" << std::endl;
	clock_t t = clock();
	for (int i = 0; i < 1000; ++i) {
		cache.set(i, i + 10, i + 100);
	}
	std::cout << "set 1000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();
	for (int i = 0; i < 5000; ++i) {
		cache.set(i, i + 10, i + 100);
	}
	std::cout << "set 5000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();
	for (int i = 0; i < 10000; ++i) {
		cache.set(i, i + 10, i + 100);
	}
	std::cout << "set 10000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();	
	for (int i = 0; i < 20000; ++i) {
		cache.set(i, i + 10, i + 100);
	}
	std::cout << "set 20000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	std::cout << "\nGET TIME TEST:" << std::endl;
	std::vector<int> v(1, 0);
	std::vector<time_t> ti(1, 0);
	t = clock();	
	for (int i = 0; i < 1000; ++i) {
		cache.get(3, &v[0], &ti[0]);
	}
	std::cout << "get 1000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();	
	for (int i = 0; i < 5000; ++i) {
		cache.get(3, &v[0], &ti[0]);
	}
	std::cout << "get 5000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();	
	for (int i = 0; i < 10000; ++i) {
		cache.get(3, &v[0], &ti[0]);
	}
	std::cout << "get 10000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	t = clock();	
	for (int i = 0; i < 20000; ++i) {
		cache.get(3, &v[0], &ti[0]);
	}
	std::cout << "get 20000: " << ((float)(clock()-t))/CLOCKS_PER_SEC << " sec" << std::endl;
	
}
