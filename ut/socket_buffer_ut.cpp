#include "gtest/gtest.h"
#include "buffer.h"

TEST(SocketWBuffer, WriteField) {
	int fd[2];
	pipe(fd);
	SocketWBuffer wbuf(4, fd[1]);
	wbuf.WriteField("NOT_FOUND\r\n\0");
	wbuf.Flush();
	std::vector<char> vc(20);
	read(fd[0], vc.data(), 20);
	std::string s(vc.data());
	EXPECT_EQ("NOT_FOUND\r\n", s);
	wbuf.WriteField("NOT_FOUND1234567890\0");
	wbuf.Flush();
	read(fd[0], vc.data(), 20);
	std::string s1(vc.data());
	EXPECT_EQ("NOT_FOUND1234567890", s1);
}

TEST(SocketRBuffer, ReadField) {
	int fd[2];
	pipe(fd);
	std::string data = "TEST1 TEST2 TEST3\0";
	write(fd[1], data.c_str(), 18);
	SocketRBuffer rbuf(4, fd[0]);
	std::string s = rbuf.ReadField('3');
	EXPECT_EQ("TEST1 TEST2 TEST", s);
}
