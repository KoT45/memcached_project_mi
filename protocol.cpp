#include "protocol.h"


MC_COMMAND CommandName2Code(std::string& param) {
    if (param == "set") {
        return CMD_SET;
    } else if (param == "add") {
        return CMD_ADD;
    } else if (param == "get") {
        return CMD_GET;
    } else if (param == "delete") {
        return CMD_DELETE;
    } else {
		throw std::runtime_error("Invalid command name \"" + param + "\"");
        return CMD_UNKNOWN;
    }
}


std::string ResultCode2String(MC_RESULT_CODE code) {
    switch (code) {
    case R_STORED:
        {
            return "STORED";
            break;
        }
    case R_NOT_STORED:
        {
            return "NOT_STORED";
            break;
        }
    case R_EXISTS:
        {
            return "EXISTS";
            break;
        }
    case R_NOT_FOUND:
        {
            return "NOT_FOUND";
            break;
        }
    case R_DELETED:
        {
            return "DELETED";
            break;
        }
    default:
	    {
			throw std::runtime_error("Invalid result code name");
	    }
    }
}


void McValue::Serialize(WBuffer* buffer) const {
	buffer->WriteField(key_, ' ');
	buffer->WriteUint32(flags_);
	buffer->WriteChar(' ');
	buffer->WriteUint32(exp_time_);
	buffer->WriteChar(' ');
	buffer->WriteUint32(data_.size());
	buffer->WriteField("\r\n");
	buffer->WriteBytes(data_);
	buffer->WriteField("\r\n");
}


void McCommand::Deserialize(RBuffer* buffer) {
    std::string cmd = buffer->ReadField(' ');
	buffer->ReadChar();
	command = CommandName2Code(cmd);
	if (command == CMD_GET) {
		std::string s = buffer->ReadField('\r');
		boost::split(keys, s, boost::is_any_of(" "), boost::token_compress_on);
	} else if (command == CMD_DELETE) {
		std::string s = buffer->ReadField('\r');
		boost::split(keys, s, boost::is_any_of(" "), boost::token_compress_on);			
	} else {
		keys.push_back(buffer->ReadField(' '));
		buffer->ReadChar();
		flags = buffer->ReadUint32();
		buffer->ReadChar();
		exp_time = (time_t) buffer->ReadUint32();
		buffer->ReadChar();
		
		int32_t n = buffer->ReadUint32();
		buffer->ReadCharCheck('\r');
		buffer->ReadCharCheck('\n');
	
		data = buffer->ReadBytes(n);
	}
	buffer->ReadCharCheck('\r');
    buffer->ReadCharCheck('\n');
}


void McResult::Serialize(WBuffer* buffer) const{
    switch(type_) {
	case RT_ERROR:
	{
		buffer->WriteField("ERROR ");
		buffer->WriteField(error_message_);
		break;
	}
	case RT_CODE:
	{
		buffer->WriteField(ResultCode2String(code_));
		break;
	}
	case RT_VALUE:
	{
		for (McValue elem: values_) {
			buffer->WriteField("VALUE ");
			elem.Serialize(buffer);
		}
		buffer->WriteField("END");
		break;
	}
	default:
	{
		throw std::runtime_error("Invalid result type");
		break;
		}
    }
	buffer->WriteField("\r\n");
	buffer->Flush();
}


McResult ProcessCommand(const McCommand& command, LRU_cache<std::string, std::pair<unsigned int, std::vector<char>>> &cache) {	
	switch(command.command) {
	case CMD_SET: {
		cache.set(command.keys[0], command.exp_time, std::make_pair(command.flags, command.data));
		return (McResult(R_STORED));
	}	
	case CMD_ADD: {
		std::pair<unsigned int, std::vector<char>> result;
		time_t exp_time;
		if (!cache.get(command.keys[0], &result, &exp_time)) {
			cache.set(command.keys[0], command.exp_time, std::make_pair(command.flags, command.data));
			return (McResult(R_STORED));
		} else {
			return (McResult(R_NOT_STORED));
		}
	}
	case CMD_GET: {
		std::vector<McValue> v;
		for (std::string key : command.keys) {
			std::pair<unsigned int, std::vector<char>> result;
			time_t exp_time;
			if (cache.get(key, &result, &exp_time))
				v.emplace_back(key, result.first, exp_time, result.second);
		}
		return (McResult(v));
	}
	case CMD_DELETE: {
		if (cache.del(command.keys[0])) {
			return (McResult(R_DELETED));
		} else {
			return (McResult(R_NOT_FOUND));   
		}
	}
	default: {
		return (McResult("Command not found."));
	}
	}
}


void ProcessConnection(int conn_fd, LRU_cache<std::string, std::pair<unsigned int, std::vector<char>>> &cache, std::set<int> *fds, std::mutex &m) {
	try {
		SocketRBuffer rbuf(4096, conn_fd);
		SocketWBuffer wbuf(4096, conn_fd);
		McCommand command;
		command.Deserialize(&rbuf);
		McResult RecData = ProcessCommand(command, cache);
		RecData.Serialize(&wbuf);
	} catch (const std::exception &ex) {
		std::cout << ex.what() << std::endl;
	}
    m.lock();
    fds->erase(conn_fd);
    m.unlock();
    
}
