#pragma once

#include <unordered_map>
#include <list>
#include <stdexcept>
#include <ctime>
#include <iostream>
#include <boost/thread.hpp>

template<typename Key_t, typename Value_t>
class LRU_cache {

private:
	typedef std::pair<Key_t, std::pair<Value_t, time_t>> Node;
	size_t max_size;
	std::list<Node> cache;
	typename std::unordered_map<Key_t, typename std::list<Node>::iterator> hash; 
//	typename std::unordered_map<Key_t, boost::mutex> mutexies;
	boost::mutex mutex_del, mutex_acc;
public:
	LRU_cache(size_t max_size);
	//~LRU_cache();
	void set(Key_t key, time_t exp_time, Value_t value);
	bool get(Key_t key, Value_t* value, time_t* exp_time);
	bool del(Key_t key);
	void print();
};


template<typename Key_t, typename Value_t>
LRU_cache<Key_t, Value_t>::LRU_cache(size_t max_size) : max_size(max_size) {
}


template<typename Key_t, typename Value_t>
void LRU_cache<Key_t, Value_t>::set(Key_t key, time_t exp_time, Value_t value) {
	boost::unique_lock<boost::mutex> lock(mutex_acc);
	auto got = hash.find(key);
	if (got != hash.end()) {
		cache.erase(got->second);
	//	hash.erase(key);
	} else {
		while (cache.size() >= max_size) {
			auto it=cache.back();
			hash.erase(it.first);
			cache.pop_back();
		}
	}
	if (exp_time <= 60*60*24*30) {
        exp_time += time(nullptr);
	}
    cache.push_front(std::make_pair(key, std::make_pair(value, exp_time)));
    hash[key]=cache.begin();
}


template<typename Key_t, typename Value_t>
bool LRU_cache<Key_t, Value_t>::get(Key_t key, Value_t* value, time_t* exp_time) {
	boost::unique_lock<boost::mutex> lock(mutex_acc);
	auto got = hash.find(key);
	if (got != hash.end()) {
		*value = got->second->second.first;
		*exp_time = got->second->second.second;
		cache.erase(got->second);
		cache.push_front(std::make_pair(key, std::make_pair(*value, *exp_time)));
		hash[key]=cache.begin();
		return true;
	} else {
		return false;
	}
}


template<typename Key_t, typename Value_t>
bool LRU_cache<Key_t, Value_t>::del(Key_t key) {
    boost::unique_lock<boost::mutex> lock(mutex_acc);
	auto got = hash.find(key);
	if (got != hash.end()) {
		cache.erase(got->second);
		hash.erase(key);
		return true;
	} else {
		return false;
	}
}


template<typename Key_t, typename Value_t>
void LRU_cache<Key_t, Value_t>::print() {
	boost::unique_lock<boost::mutex> lock(mutex_acc);
	std::cout <<"LRU_CACHE\n";	
	std::cout <<"key    value   exp_time\n";
	for (auto it : cache) {
		std::cout << it.first << ' ' << it.second.first << ' ' << it.second.second << '\n';
	}
}
